# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
	num = n ^ 3
	primeiro = 1
	soma = 1
	while soma != num
		primeiro += 2
		soma = primeiro
		impar = primeiro + 2
		i = 1
		while i < n
			soma = soma + impar
			impar += 2
			i += 1
		end
	end
	return primeiro	
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
	num = m ^ 3
	primeiro = 1
	soma = 1
	print(m, " ")
	print(num, " ")
	while soma != num
		primeiro += 2
		soma = primeiro
		impar = primeiro + 2
		i = 1
		while i < m
			soma = soma + impar
			impar += 2
			i += 1
		end
	end
	print(primeiro, " ")
	proximo = primeiro + 2
	i = 1
	while i < m
		print(proximo, " ")
		proximo += 2
		i += 1
	end
end

function mostra_n(n)
	m = 1
	while m <= n
		imprime_impares_consecutivos(m)
		println("")
		m += 1
	end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        println("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        println("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(10) != 91
    	println("Sua função retornou o valor errado para n = 10")
    end
    if impares_consecutivos(7) != 43
        println("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        println("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        println("Sua função retornou o valor errado para n = 21")
    end
    if impares_consecutivos(20) != 381
    	println("Sua função retornou o valor errado para n = 20")
    end
    println("Acabaram os testes")
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
#test() 
